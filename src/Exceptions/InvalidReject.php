<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * The requested email is not in the rejection list
 */
class InvalidReject extends MandrillError
{

}