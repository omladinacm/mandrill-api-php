<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * Non-empty pools cannot be deleted.
 */
class InvalidDeleteNonEmptyPool extends MandrillError
{

}