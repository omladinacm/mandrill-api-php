<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * The provided subaccount id does not exist.
 */
class UnknownSubaccount extends MandrillError
{

}