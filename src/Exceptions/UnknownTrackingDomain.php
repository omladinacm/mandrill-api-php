<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * The provided tracking domain does not exist.
 */
class UnknownTrackingDomain extends MandrillError
{

}