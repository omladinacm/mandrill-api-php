<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * The provided message id does not exist.
 */
class UnknownMessage extends MandrillError
{

}