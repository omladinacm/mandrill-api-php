<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * The provided inbound route does not exist.
 */
class UnknownInboundRoute extends MandrillError
{

}