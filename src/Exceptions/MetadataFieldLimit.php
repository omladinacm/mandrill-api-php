<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * Custom metadata field limit reached.
 */
class MetadataFieldLimit extends MandrillError
{

}