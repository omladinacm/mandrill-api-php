<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * The requested tag does not exist or contains invalid characters
 */
class InvalidTagName extends MandrillError
{

}