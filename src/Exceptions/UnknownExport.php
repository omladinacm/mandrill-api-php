<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * The requested export job does not exist
 */
class UnknownExport extends MandrillError
{

}