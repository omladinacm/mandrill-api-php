<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * The requested URL has not been seen in a tracked link
 */
class UnknownURL extends MandrillError
{

}