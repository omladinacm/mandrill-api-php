<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * The given template name already exists or contains invalid characters
 */
class InvalidTemplate extends MandrillError
{

}