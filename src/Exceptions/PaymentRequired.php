<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * The requested feature requires payment.
 */
class PaymentRequired extends MandrillError
{

}