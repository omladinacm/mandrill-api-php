<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * The provided dedicated IP does not exist.
 */
class UnknownIP extends MandrillError
{

}