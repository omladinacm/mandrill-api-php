<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * The requested webhook does not exist
 */
class UnknownWebhook extends MandrillError
{

}