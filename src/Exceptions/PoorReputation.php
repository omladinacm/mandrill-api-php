<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * The user's reputation is too low to continue.
 */
class PoorReputation extends MandrillError
{

}