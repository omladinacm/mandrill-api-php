<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * The domain name is not configured for use as the dedicated IP's custom reverse DNS.
 */
class InvalidCustomDNSPending extends MandrillError
{

}