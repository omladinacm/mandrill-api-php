<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * The subsystem providing this API call is down for maintenance
 */
class ServiceUnavailable extends MandrillError
{

}