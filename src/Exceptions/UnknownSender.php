<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * The requested sender does not exist
 */
class UnknownSender extends MandrillError
{

}