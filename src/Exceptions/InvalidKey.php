<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * The provided API key is not a valid Mandrill API key
 */
class InvalidKey extends MandrillError
{

}