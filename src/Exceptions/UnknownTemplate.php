<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * The requested template does not exist
 */
class UnknownTemplate extends MandrillError
{

}