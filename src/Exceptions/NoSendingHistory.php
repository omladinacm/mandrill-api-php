<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * The user hasn't started sending yet.
 */
class NoSendingHistory extends MandrillError
{

}