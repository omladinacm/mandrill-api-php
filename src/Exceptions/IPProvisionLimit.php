<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * A dedicated IP cannot be provisioned while another request is pending.
 */
class IPProvisionLimit extends MandrillError
{

}