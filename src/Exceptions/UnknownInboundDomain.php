<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * The requested inbound domain does not exist
 */
class UnknownInboundDomain extends MandrillError
{

}