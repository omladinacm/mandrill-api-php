<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * The default pool cannot be deleted.
 */
class InvalidDeleteDefaultPool extends MandrillError
{

}