<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * The provided metadata field name does not exist.
 */
class UnknownMetadataField extends MandrillError
{

}