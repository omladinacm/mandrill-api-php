<?php


namespace Omladinacm\Mandrill\Exceptions;


/**
 * The provided dedicated IP pool does not exist.
 */
class UnknownPool extends MandrillError
{

}