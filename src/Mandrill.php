<?php


namespace Omladinacm\Mandrill;


use Omladinacm\Mandrill\Endpoints\Exports;
use Omladinacm\Mandrill\Endpoints\Inbound;
use Omladinacm\Mandrill\Endpoints\Internal;
use Omladinacm\Mandrill\Endpoints\Ips;
use Omladinacm\Mandrill\Endpoints\Messages;
use Omladinacm\Mandrill\Endpoints\Metadata;
use Omladinacm\Mandrill\Endpoints\Rejects;
use Omladinacm\Mandrill\Endpoints\Senders;
use Omladinacm\Mandrill\Endpoints\Subaccounts;
use Omladinacm\Mandrill\Endpoints\Tags;
use Omladinacm\Mandrill\Endpoints\Templates;
use Omladinacm\Mandrill\Endpoints\Urls;
use Omladinacm\Mandrill\Endpoints\Users;
use Omladinacm\Mandrill\Endpoints\Webhooks;
use Omladinacm\Mandrill\Endpoints\Whitelists;
use Omladinacm\Mandrill\Exceptions\MandrillError;
use Omladinacm\Mandrill\Exceptions\HttpError;

class Mandrill
{
    public $apikey;
    public $ch;
    public $root = 'https://mandrillapp.com/api/1.0';
    public $debug = false;

    public static $error_map = array(
        "ValidationError" => "ValidationError",
        "Invalid_Key" => "InvalidKey",
        "PaymentRequired" => "PaymentRequired",
        "Unknown_Subaccount" => "UnknownSubaccount",
        "Unknown_Template" => "UnknownTemplate",
        "ServiceUnavailable" => "ServiceUnavailable",
        "Unknown_Message" => "UnknownMessage",
        "Invalid_Tag_Name" => "InvalidTagName",
        "Invalid_Reject" => "InvalidReject",
        "Unknown_Sender" => "UnknownSender",
        "Unknown_Url" => "UnknownURL",
        "Unknown_TrackingDomain" => "UnknownTrackingDomain",
        "Invalid_Template" => "InvalidTemplate",
        "Unknown_Webhook" => "UnknownWebhook",
        "Unknown_InboundDomain" => "UnknownInboundDomain",
        "Unknown_InboundRoute" => "UnknownInboundRoute",
        "Unknown_Export" => "UnknownExport",
        "IP_ProvisionLimit" => "IPProvisionLimit",
        "Unknown_Pool" => "Unknown_Pool",
        "NoSendingHistory" => "NoSendingHistory",
        "PoorReputation" => "PoorReputation",
        "Unknown_IP" => "UnknownIP",
        "Invalid_EmptyDefaultPool" => "InvalidEmptyDefaultPool",
        "Invalid_DeleteDefaultPool" => "InvalidDeleteDefaultPool",
        "Invalid_DeleteNonEmptyPool" => "InvalidDeleteNonEmptyPool",
        "Invalid_CustomDNS" => "InvalidCustomDNS",
        "Invalid_CustomDNSPending" => "InvalidCustomDNSPending",
        "Metadata_FieldLimit" => "MetadataFieldLimit",
        "Unknown_MetadataField" => "UnknownMetadataField"
    );

    /**
     * @var \Omladinacm\Mandrill\Endpoints\Templates
     */
    public $templates;
    /**
     * @var \Omladinacm\Mandrill\Endpoints\Exports
     */
    public $exports;
    /**
     * @var \Omladinacm\Mandrill\Endpoints\Users
     */
    public $users;
    /**
     * @var \Omladinacm\Mandrill\Endpoints\Rejects
     */
    public $rejects;
    /**
     * @var \Omladinacm\Mandrill\Endpoints\Inbound
     */
    public $inbound;
    /**
     * @var \Omladinacm\Mandrill\Endpoints\Tags
     */
    public $tags;
    /**
     * @var \Omladinacm\Mandrill\Endpoints\Messages
     */
    public $messages;
    /**
     * @var \Omladinacm\Mandrill\Endpoints\Whitelists
     */
    public $whitelists;
    /**
     * @var \Omladinacm\Mandrill\Endpoints\Ips
     */
    public $ips;
    /**
     * @var \Omladinacm\Mandrill\Endpoints\Internal
     */
    public $internal;
    /**
     * @var \Omladinacm\Mandrill\Endpoints\Subaccounts
     */
    public $subaccounts;
    /**
     * @var \Omladinacm\Mandrill\Endpoints\Urls
     */
    public $urls;
    /**
     * @var \Omladinacm\Mandrill\Endpoints\Webhooks
     */
    public $webhooks;
    /**
     * @var \Omladinacm\Mandrill\Endpoints\Senders
     */
    public $senders;
    /**
     * @var \Omladinacm\Mandrill\Endpoints\Metadata
     */
    public $metadata;

    /**
     * Mandrill constructor.
     *
     * @param null $apikey
     *
     * @throws \Omladinacm\Mandrill\Exceptions\MandrillError
     */
    public function __construct($apikey=null) {
        if(!$apikey) $apikey = getenv('MANDRILL_APIKEY');
        if(!$apikey) $apikey = $this->readConfigs();
        if(!$apikey) throw new MandrillError('You must provide a Mandrill API key');
        $this->apikey = $apikey;

        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_USERAGENT, 'Omladinacm-Mandrill/1.0');
        curl_setopt($this->ch, CURLOPT_POST, true);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($this->ch, CURLOPT_HEADER, false);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 600);

        $this->root = rtrim($this->root, '/') . '/';

        $this->templates = new Templates($this);
        $this->exports = new Exports($this);
        $this->users = new Users($this);
        $this->rejects = new Rejects($this);
        $this->inbound = new Inbound($this);
        $this->tags = new Tags($this);
        $this->messages = new Messages($this);
        $this->whitelists = new Whitelists($this);
        $this->ips = new Ips($this);
        $this->internal = new Internal($this);
        $this->subaccounts = new Subaccounts($this);
        $this->urls = new Urls($this);
        $this->webhooks = new Webhooks($this);
        $this->senders = new Senders($this);
        $this->metadata = new Metadata($this);
    }

    /**
     * Class destructor
     */
    public function __destruct() {
        curl_close($this->ch);
    }

    /**
     * Call Mandrill API for requested endpoint
     *
     * @param $url
     * @param $params
     *
     * @return mixed
     * @throws \Omladinacm\Mandrill\Exceptions\MandrillError
     * @throws \Omladinacm\Mandrill\Exceptions\HttpError
     */
    public function call($url, $params) {
        $params['key'] = $this->apikey;
        $params = json_encode($params);
        $ch = $this->ch;

        curl_setopt($ch, CURLOPT_URL, $this->root . $url . '.json');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_VERBOSE, $this->debug);

        $start = microtime(true);
        $this->log('Call to ' . $this->root . $url . '.json: ' . $params);
        if($this->debug) {
            $curl_buffer = fopen('php://memory', 'w+');
            curl_setopt($ch, CURLOPT_STDERR, $curl_buffer);
        }

        $response_body = curl_exec($ch);
        $info = curl_getinfo($ch);
        $time = microtime(true) - $start;
        if($this->debug) {
            rewind($curl_buffer);
            $this->log(stream_get_contents($curl_buffer));
            fclose($curl_buffer);
        }
        $this->log('Completed in ' . number_format($time * 1000, 2) . 'ms');
        $this->log('Got response: ' . $response_body);

        if(curl_error($ch)) {
            throw new HttpError("API call to $url failed: " . curl_error($ch));
        }
        $result = json_decode($response_body, true);
        if($result === null) {
            throw new MandrillError('We were unable to decode the JSON response from the Mandrill API: ' . $response_body);
        }

        if(floor($info['http_code'] / 100) >= 4) {
            throw $this->castError($result);
        }

        return $result;
    }

    public function readConfigs() {
        $paths = array('~/.mandrill.key', '/etc/mandrill.key');
        foreach($paths as $path) {
            if(file_exists($path)) {
                $apikey = trim(file_get_contents($path));
                if($apikey) return $apikey;
            }
        }
        return false;
    }

    /**
     * @param $result
     *
     * @return \Omladinacm\Mandrill\Exceptions\MandrillError|mixed
     * @throws \Omladinacm\Mandrill\Exceptions\MandrillError
     */
    public function castError($result) {
        if($result['status'] !== 'error' || !$result['name']) {
            throw new MandrillError('We received an unexpected error: ' . json_encode($result));
        }

        $class = (isset(self::$error_map[$result['name']])) ? self::$error_map[$result['name']] : 'MandrillError';
        return new $class($result['message'], $result['code']);
    }

    /**
     * Logs message if debug mode is enabled
     *
     * @param $msg
     */
    public function log($msg) {
        if($this->debug) {
            error_log($msg);
        }
    }
}